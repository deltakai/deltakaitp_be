package com.deltakai.dojo.model

import javax.persistence.*
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder


@Entity
 class Users {
    @Id
    lateinit var userName: String
    var password: String = ""
        get() = field
        set(value) {
            val passwordEncoder = BCryptPasswordEncoder()
            field = passwordEncoder.encode(value)
        }
    var isValid: Long = 0
   fun checkPassword(password:String):Boolean = BCryptPasswordEncoder().matches(password, this.password)
}
