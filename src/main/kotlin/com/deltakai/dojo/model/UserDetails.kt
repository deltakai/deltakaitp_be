package com.deltakai.dojo.model

import org.hibernate.Hibernate
import javax.persistence.*
import javax.persistence.GenerationType.AUTO
import javax.persistence.GenerationType.TABLE

@Entity
data class UserDetails(
    @Id
    @GeneratedValue(strategy = TABLE)
    val userId: Long?=0,
    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "userName", nullable = false)
    var users: Users,
      var firstName: String,
      var lastName: String,
      var phoneNumber: String,
      var emailId: String,
      var gender: String,
      var company: String,
      var country: String,
    var password:String,
      var isEmailVerified: String="0",
    var forceChangePassword: String ="0"
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || Hibernate.getClass(this) != Hibernate.getClass(other)) return false
        other as UserDetails

        return userId != null && userId == other.userId
    }

    override fun hashCode(): Int = 0

    @Override
    override fun toString(): String {
        return this::class.simpleName + "(userId = $userId , firstName = $firstName , lastName = $lastName , phoneNumber = $phoneNumber , emailId = $emailId , gender = $gender , company = $company , country = $country , password = $password , isEmailVerified = $isEmailVerified , forceChangePassword = $forceChangePassword )"
    }
}