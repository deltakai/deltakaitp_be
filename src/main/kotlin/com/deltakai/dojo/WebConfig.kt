package com.deltakai.dojo

import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.CorsRegistry
import org.springframework.web.servlet.config.annotation.EnableWebMvc
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer


@Configuration //to make configuration available to every route
@EnableWebMvc //to apply to every controller
//to handle frontend CORS problems and handle credentials
class WebConfig:WebMvcConfigurer {
    override fun addCorsMappings(registry: CorsRegistry) {
        registry.addMapping("/**") //every route that we have will match this route
            .allowedOrigins("*") //to allow communication with frontend React framework
            .allowedMethods("GET", "PUT", "POST", "PATCH", "DELETE", "OPTIONS")
            //.allowCredentials(true) //important for frontend to receive cookie
    }
}