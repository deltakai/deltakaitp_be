package com.deltakai.dojo.repository

import com.deltakai.dojo.model.UserDetails
import com.deltakai.dojo.model.Users
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.lang.Nullable
import org.springframework.stereotype.Repository

@Repository
@EnableJpaRepositories
interface  UserDetailsRepository : JpaRepository<UserDetails, Long> {

    abstract fun findByEmailId(@Nullable emailId: String) : UserDetails

}