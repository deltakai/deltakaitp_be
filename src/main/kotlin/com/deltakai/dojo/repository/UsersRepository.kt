package com.deltakai.dojo.repository

import com.deltakai.dojo.model.Users
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface  UsersRepository : JpaRepository<Users, Long> {

    fun  findByUserName(userName: String) : Users

}