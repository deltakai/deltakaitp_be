package com.deltakai.dojo.controller


import com.deltakai.dojo.DTO.UserLoginDTO
import com.deltakai.dojo.model.UserDetails
import com.deltakai.dojo.model.Users
import com.deltakai.dojo.repository.UserDetailsRepository
import com.deltakai.dojo.repository.UsersRepository

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder

import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.*
import javax.servlet.http.Cookie
import javax.servlet.http.HttpServletResponse
import kotlin.collections.HashMap


@RestController
@RequestMapping("/api/user")
class RegistrationController {
    @Autowired
    lateinit var userDetailsRep: UserDetailsRepository

    @Autowired
    lateinit var usersRepository: UsersRepository
    lateinit var users: Users
    lateinit var userDetails: UserDetails

    @PostMapping("/userRegistration")
    fun createUser(@RequestBody userDetailsUI: UserDetails): ResponseEntity<Map<String, String>> {
        userDetails=userDetailsUI
        var userName: String = userDetails.users.userName

        print("user is  $userName")
        val myMap: HashMap<String, String> = HashMap<String, String>()
        usersRepository.save(userDetails.users)
        userDetailsRep.save(userDetails)
        myMap["Registration"] = "Success"
        myMap["UserName"] = userName
        return ResponseEntity.ok(myMap)
    }

    @PostMapping("/login")
    fun loginUser(@RequestBody body: UserLoginDTO, response: HttpServletResponse): HashMap<String, String> {
        val myMap: HashMap<String, String> = HashMap<String, String>()
        val users = usersRepository.findByUserName(body.userName) ?: return throw IllegalArgumentException("User Not Found")

        if(!users.checkPassword(body.password)){
            myMap["isPassword"] = "Invalid"
            myMap["LoginStatus"] = "Fail"
            return myMap
        }else{
            myMap["isPassword"] = "Valid"
            myMap["LoginStatus"] = "Success"
            return myMap
        }

        //val issuer = users.userName

        /*val jwt = Jwts.builder()
            .setIssuer(issuer)
            .setExpiration(Date(System.currentTimeMillis()+60*24*1000))
            .signWith(SignatureAlgorithm.HS256,secretKey)
            .compact()


        val cookie = Cookie("jwt",jwt)
        cookie.isHttpOnly = true //only can be used by backend for validation

        response.addCookie(cookie)*/

       // return ResponseEntity.ok("success")
    }
}