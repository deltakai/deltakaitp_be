package com.deltakai.dojo.service

import com.deltakai.dojo.model.UserDetails
import com.deltakai.dojo.model.Users
import com.deltakai.dojo.repository.UserDetailsRepository
import com.deltakai.dojo.repository.UsersRepository
import org.springframework.data.jpa.repository.Query


class UsersService(private val usersRepository: UsersRepository) {

    fun save(users: Users): Users = usersRepository.save(users)
   // @Query("SELECT count(Users.userName) FROM Users WHERE Users.userName = :users")
    @Query(value="SELECT * FROM Users  WHERE Users.userName = :users",nativeQuery = true)
    fun findByUserName(users: String): Users = usersRepository.findByUserName(users)

   // fun findByUsername(userName: String): Users? = this.usersRepository.findByUsername(userName)


}