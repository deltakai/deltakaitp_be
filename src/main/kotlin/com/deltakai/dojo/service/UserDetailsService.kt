package com.deltakai.dojo.service

import com.deltakai.dojo.model.UserDetails
import com.deltakai.dojo.model.Users
import com.deltakai.dojo.repository.UserDetailsRepository
import org.springframework.data.jpa.repository.Query

class UserDetailsService(private val userDetailsRepository: UserDetailsRepository) {

    fun save(userdata: UserDetails): UserDetails = userDetailsRepository.save(userdata)

    @Query("FROM UserDetails.emailId WHERE UserDetails.emailId = :emailId")
    fun findByEmailId(emailId: String): UserDetails = userDetailsRepository.findByEmailId(emailId)

}